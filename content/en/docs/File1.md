---
title: "Step 1 of Retail Integration"
linkTitle: "Step 1- Identify Primary Point of Contact"
weight: 300
date: 2022-06-16
mermaid: true
description: >
  Identify Primary Point of Contact
---

Below mentioned **TPM** will be your primary point of contact throughout the implementation. 

Please leverage your TPM for any implementation issues, technical support and to answer any queries to better understand our solution.