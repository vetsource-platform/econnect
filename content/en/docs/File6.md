---
title: "Step 6 of Retail Integration"
linkTitle: "Step 6 - Retail Communication Touch-points"
weight: 399.9
date: 2022-06-16
mermaid: true
description: >
  Retail Communication Touch-points during the Order Journey
---

During the life cycle of the order, order status and line item status will change during different stages which will be visible through the API.  
Based upon these statuses different messages could be triggered to Pet Parents so that they are well informed about the current status of their order.  

An **Order** could be in one of the following **Statuses**:
1.      Unprocessed  
2.      Processing
3.      Shipped
4.      Cancelled

**Line Items** could be in one of the following **statuses**:
1.     Communications Failure
2.     Awaiting Rx
3.     Rejected
4.     Processing
5.     Waiting on Inventory
6.     Shipped
7.     ReShipped
8.     Unprocessed
9.     Cancelled

Communication trigger points and suggested communication details are shared in the following document.

[Email Touch Points and Notifications](https://docs.google.com/spreadsheets/d/1zQjUNStKHvaHx6YLMOY_1c90esVKfzOA/edit?usp=sharing&ouid=116278783390683079565&rtpof=true&sd=true)

[Cancel and Denial Reason Codes for Notifications](https://docs.google.com/spreadsheets/d/1zQjUNStKHvaHx6YLMOY_1c90esVKfzOA/edit?usp=sharing&ouid=116278783390683079565&rtpof=true&sd=true)


**Pet Weight** and **Clinic ID** could change after the order is created. Partners will need to consume these updates.
In addition to order and line item status changes we will also provide updated pet weight and assigned clinic information if those change for the order.
Partner should look at the response for:
1. Pet Weight : orders/{id} or orders/{id}/status or orders/changed and if pet weight is changed after order submission then it should be updated on partner side.
2. Clinic ID : orders/{id} or orders/changed and if the Clinic ID is changed after order submission then it should be updated on partner side.

**Prescription Modification** When a clinic denies an order due to incorrect product, they have the option of suggesting the correct product.
1. Please contact your Account Manager / TPM to enable this feature.
2. Partners should consume the suggested product from the orders/{id} or orders/{id}/status or orders/changed and communicate to the Pet Parents that their order was denied, but the correct product was approved and is ready for them to reorder.











  


