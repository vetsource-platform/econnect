---
title: " "
linkTitle: " "
weight: 200
date: 2022-01-26
mermaid: true
menu:
  main:
    weight: 200
---


Congratulations on your decision to partner with Vetsource to leverage Electronic prescription approval solution integrated through APIs. This document will provide your Engineering Team step by step instructions on how to integrate with our eConnect product.

**What is Vetsource Retail?**

Vetsource Retail provides pharmacy services for online retailers while keeping the veterinarian in the information loop for the approval process.

**What is Retail Business Process?**

An Online retailer partners with Vetsource. Customers then purchase prescription products from the retailer’s website or a Vetsource hosted hospital ecommerce site where they are prompted to enter pet details and choose their veterinarian from a predetermined list. If their veterinarian is not found, they are prompted to enter a new one.

Once the order is received, Vetsource will then contact the veterinarian for prescription approval and provides approval/denial updates to the online retailer. Based upon the agreement, order fulfillment is performed by either Vetsource or the Retail Partner.

<a id="eConnectOrderFlow" style="visibility:hidden;"></a>
<a href="images/eConnectOrderFlow.png"><img src="images/eConnectOrderFlow.png" align="center" style="max-width:100%;padding-top:20px;"></a>





**How to integrate with Retail products from Vetsource?**

Your Engineering team  can easily integrate with Vetsource in 7 simple easy to follow steps provided below.

Vetsource Team is here to help, so please don’t hesitate to reach out to them, especially the TPM assigned to you, if you are stuck at any step or need any help.
