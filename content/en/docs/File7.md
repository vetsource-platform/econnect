---
title: "Step 7 of Retail Integration"
linkTitle: "Step 7 - Key APIs"
weight: 399.9
date: 2022-06-16
mermaid: true
description: >
   Key APIs
---
As shared in API Order Journey under **Step 3** (also shared ar bottom of this page) there are various APIs which will be required for end to end implementation.   

But there are 4 which are critical for successful implementation and their details are provided below. Please leverage your assigned TPM to get a better understanding of all these APIs.

1. **Veterinarian Search API**: To get list of practices matching search criteria from Vetsource
2. **Complete Catalog & Delta Catalog Export API** : To get delta of changes made from last catalog export
3. **TAX API** : For calculating Tax on Medication Items 
4. **Monograph API**: To fetch Monograph pdf files for Medication items shipped by Vetsource

Link of detail level API documentation: [/Retail API Documentation/](/openapi/#/)

**Retail API calls : Initial Readiness**


1. **Catalog export**: [/catalog/exportCompleteCatalog/extended](/openapi/#/catalog/exportCompleteCatalogExtended)   
   Type of Request: Get  
   Frequency: 1 time before go live.



2. **Veterinarian / Practice Search**: [/vets/search/enhanced](/openapi/#/practice/get_vets_search_enhanced)  
   Type of Request: Get  
   Frequency: Every time a Vet practice needs to be searched for line item. 


**Retail API calls : Day to Day Transactional Work**

1. **Catalog changes delta export**: [/catalog/exportCatalog/enhanced](/openapi/#/catalog/exportCatalogExtended)  
   Type of Request: Get  
   Frequency: Every night


2. **Create Order**: [/orders/createOrder](/openapi/#/orders/createOrder)  
   Type of Request: Put  
   Frequency: Anytime an order needs to be created.


3. **Find Orders changed**: [/orders/changed](/openapi/#/orders/getChangedOrders)  
   Type of Request: Get  
   Frequency: 15 minutes or more interval.


4. **Cancel complete Order**: [/orders/{id}/cancel](/openapi/#/orders/cancelOrder)  
   Type of Request: Delete  
   Frequency: Only once for an Order

   
5. **Cancel line items of an Order**: [/orders/{id}/cancelline/{lineNumber}/reason/{reason}](/openapi/#/orders/cancelItem)  
   Type of Request: Delete  
   Frequency: Only once for per line item of an Order


6. **Calculate Sales Tax for Medication line items in an Order**: [/orders/tax](/openapi/#/orders/calculateSalesTax)  
   Type of Request: Post  
   Frequency: Only once for an Order

   PS: This API provides Tax information for Medication Items only


7. **Monograph PDF Files for an Order with Medication Items**: [/orders/{id}/monographs](/openapi/#/orders/getMonographsForOrder)  
   Type of Request: Get  
   Frequency: Only once for an Order when Order is in Shipped status


8. **Update Status of Line Items for an Order shipped by Partner**: [/orders/{id}/status](/openapi/#/orders/updateOrderStatus)  
   Type of Request: Post Request  
   Frequency: Only once per Order

9. **Share Prescription image for Nutrition Items - WRX Clinics**: [/orders/{id}/linenumbers/{lineNumberId}/wrxprescription](/openapi/#/orders/uploadWrxPrescriptionFileByLineItem)  
   Type of Request: Post Request  
   Frequency: Only once per nutrition line item post order creation

10. **Mark order items as refunded**: [/orders/{id}/refund](/openapi/#/orders/refundOrderItems)  
   Type of Request: Post Request  
   Frequency: Only once per item to mark it as refunded

<a id="eConnectAPIOrderJourney" style="visibility:hidden;"></a>
<a href="../../images/econnect_API_Order_Journey.png"><img src="../../images/econnect_API_Order_Journey.png" align="left" style="max-width:125%;padding-top:0px;"></a>
  


  


















  


