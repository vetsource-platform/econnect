---
title: "Step 3 of Retail Integration"
linkTitle: "Step 3 - Understand API Calls Journey"
weight: 300
date: 2022-06-16
mermaid: true
description: >
   Understand API Calls Journey
---
Review the Pet Owner Order Journey, made through API Calls, as shared below to understand sequence of calls which will be made:


<a id="eConnectAPIOrderJourney" style="visibility:hidden;"></a>
<a href="../../images/econnect_API_Order_Journey.png"><img src="../../images/econnect_API_Order_Journey.png" align="left" style="max-width:125%;padding-top:20px;"></a>

 
  
















  


