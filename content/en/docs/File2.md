---
title: "Step 2 of Retail Integration"
linkTitle: "Step 2 - Environment Details"
weight: 300
date: 2022-06-16
mermaid: true
description: >
  Environment Details
---

Coordinate with your assigned TPM to obtain a Token, User Name, and Password.

Below mentioned Stack will be used to initiate the initial integration work.

**Test Stack URL :** http://api.rlewis-master-default.stack.securevetsource.com/partner/v1/

_**Please note Production URL will be shared before launch.**_