---
title: "Step 4.4 of Retail Integration"
linkTitle: "Step 4.4 - Retail Veterinarian Information"
weight: 390
date: 2022-01-25
mermaid: true
description: >
  Retail Veterinarian Information
---

**Veterinarian Information Guidelines:**

All the Veterinarian information will be available through API.

**API Name:** /vets/search/enhanced/\
**API Call:** https://VetsourceProvidedPath/partner/v1/vets/search/enhanced/\
**Request Type:** GET\
**Output:**  Produces search result JSON of all non-cancelled Vetsource practices matching the search criteria.\
**Recommendation:** To make call everytime the search needs to be performed.  

**Search Screen**

By consuming all the data from the above API please build the Veterinarian search on your Ecom site.

<a id="SearchScreen1" style="visibility:hidden;"></a>
<a href="../../images/SearchScreen1.PNG"><img src="../../images/SearchScreen1.PNG" align="center" style="max-width:100%;padding-top:20px;"></a>

**Veterinarian Data to be sent in API**
```xml
<vets>
   <vet>
      <id>PartnerVID001</id>
      <vetsource-clinic-id>144955</vetsource-clinic-id>
      <clinic-name>Test Hospital Lake Nona</clinic-name>
      <name>James Fargo</name>
      <address1>123 Main Street</address1>
      <address2>Ste 1234</address2>
      <city>Orlando</city>
      <state>FL</state>
      <zip>32803</zip>
      <phone>407-221-1234</phone>
      <fax>407-221-5678</fax>
   </vet>
</vets>
```

**Manual Data Entry of Veterinarian Practice**

If the veterinarian information cannot be found in the search results then pet owner should have the option to manually enter and share the information.

Data in fields mentioned below should be provided for quick and successful identification of Veterinarian practice else there might be potential delays in matching the order to right practice and may even lead to cancellation of order if no matches is found. 
1. Vet Clinic Name
2. Vet Clinic Address 
3. City
4. State
5. Zip Code
6. Vet Clinic Phone Number
7. Vet Clinic Fax

**Note:**\
**Vet Clinic Name** should always be required, then either the\
**Vet Clinic Phone Number**  
or\
**City** and **State** should be provided by Pet Owner. 

<a id="SearchScreen2" style="visibility:hidden;"></a>
<a href="../../images/SearchScreen2.PNG"><img src="../../images/SearchScreen2.PNG" align="center" style="max-width:100%;padding-top:20px;"></a>

**Veterinarian Data to be sent in API**
```xml
<vets>
   <vet>
      <id>PartnerVID002</id>
      <vetsource-clinic-id>144955</vetsource-clinic-id>
      <clinic-name>New Vet Clinic</clinic-name>
      <name>Jaine White</name>
      <address1>123 Main Street</address1>
      </address2>
      <city>Orlando</city>
      <state>FL</state>
      <zip>32803</zip>
      <phone>407-222-2221</phone>
      <fax>407-222-2222</fax>
   </vet>
</vets>
```