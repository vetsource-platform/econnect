---
title: "Step 4.2 of Retail Integration"
linkTitle: "Step 4.2 - Retail Shipment Information"
weight: 390
date: 2024-01-22
mermaid: true
description: >
   Retail Shipment Information
---

In the case of a mixed order (e.g. OTC + RX item), it should be called out that the prescription will ship separately from other products.

<a id="Ship1" style="visibility:hidden;"></a>
<a href="../../images/Ship1.PNG"><img src="../../images/Ship1.PNG" align="center" style="max-width:120%;padding-top:25px;"></a>


Shipping rates and options are variable depending on the product type and for each product shipping method should be provided through API.

<a id="Ship2" style="visibility:hidden;"></a>
<a href="../../images/ShipNew1.JPG"><img src="../../images/ShipNew1.JPG" align="center" style="max-width:120%;padding-top:25px;"></a>
<a href="../../images/ShipNew2.JPG"><img src="../../images/ShipNew2.JPG" align="center" style="max-width:120%;padding-top:25px;"></a>
<a href="../../images/ShipNew3.JPG"><img src="../../images/ShipNew3.JPG" align="center" style="max-width:120%;padding-top:25px;"></a>
<a href="../../images/ShipNew4.JPG"><img src="../../images/ShipNew4.JPG" align="center" style="max-width:120%;padding-top:25px;"></a>




Sample Shipment method codes to be sent in the API during Order creation are shared below and will be also visible in Step 5 with sample complete Order creation request body.


**Ship Method values to be sent in Order Creation API:**

If Ship Method details are provided at both Order and Line Item level then Line Item level Ship Method will override the Order level Ship Method choice. 

If **Partner** is **shipping** a product then **Ship Method** value should be passed as "**NONE**".

**For Standard Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>DEFAULT</req-ship-method>
   </line-item>
</line-items>
```
**For Next Business Day PM Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>Mail in Prescription</rx-delivery-method>
      <req-ship-method>OVERNIGHT</req-ship-method>
   </line-item>
</line-items>
```

**For Two Day Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>TWODAY</req-ship-method>
   </line-item>
</line-items>
```

**For Three Day Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>THREEDAY</req-ship-method>
   </line-item>
</line-items>
```

**For PRIORITY Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>PRIORITY</req-ship-method>
   </line-item>
</line-items>
```

**If Partner is Shipping**
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>NONE</req-ship-method>
   </line-item>
</line-items>
```