---
title: "Step 4.1 of Retail Integration"
linkTitle: "Step 4.1 - Retail Product Information"
weight: 390
date: 2022-01-25
mermaid: true
description: >
   Retail Product Information
---

**Product Information Guidelines:** 

All product information will be provided through Catalog API.

**API Name:** catalog/exportCompleteCatalog/extended\
**API Call:** https://VetsourceProvidedPath/partner/v1/catalog/exportCompleteCatalog/extended\
**Request Type:** GET\
**Output:**  Zip file of entire Catalog configured for the requested Partner.\
**Recommendation:** To consume it day before the launch and then leverage delta catalog file extract every night (details of delta catalog API are provided in "Other Key APIs" step)

<a id="Product1" style="visibility:hidden;"></a>
<a href="../../images/Product1.PNG"><img src="../../images/Product1.PNG" align="center" style="max-width:120%;padding-top:0px;"></a>



Share more details about Vetsource in **FAQ** sections.\
Discuss with the Vetsource TPM for complete information guidelines.

<a id="FAQs" style="visibility:hidden;"></a>
<a href="../../images/FAQs.PNG"><img src="../../images/FAQs.PNG" align="center" style="max-width:120%;padding-top:25px;"></a>
