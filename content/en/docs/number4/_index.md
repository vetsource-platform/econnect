---
title: "Step 4 of Retail Integration"
linkTitle: "Step 4 - Understand Retail Order Creation Components"
weight: 300
date: 2022-01-25
description: >
  Understand Retail Order Creation Components
---

One of the key part of Technical Integration is Order creation. 

Now at this step we will break down the key components of order creation information into 5 sections:

1. Product Information
2. Shipment Information
3. Pet Information
4. Veterinarian Information
5. Prescription Sharing Information