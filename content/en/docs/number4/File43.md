---
title: "Step 4.3 of Retail Integration"
linkTitle: "Step 4.3 - Retail Pet Information"
weight: 390
date: 2022-01-25
mermaid: true
description: >
  Retail Pet Information
---

Add a new step to capture Pet and Veterinarian information from the Pet Owner.

<a id="PetandVetInfo" style="visibility:hidden;"></a>
<a href="../../images/PetandVetInfo.PNG"><img src="../../images/PetandVetInfo.PNG" align="center" style="max-width:125%;padding-top:50px;"></a>


In this step we will discuss **Pet Information**, this should be captured at the **line item** level.

The following information will need to be collected when a pet owner creates a new pet profile.
This pet profile should be accessible to the pet owners account to refer and maintain for future purchases.

<a id="PetInfo" style="visibility:hidden;"></a>
<a href="../../images/PetInfo.PNG"><img src="../../images/PetInfo.PNG" align="center" style="max-width:125%;padding-top:50px;"></a>


**Pet Owner Information to be sent in API**
```xml
<pets>
   <pet>
      <id>DOG007</id>
      <name>Goofy Bond</name>
      <species>Dog</species>
      <breed>Afghan</breed>
      <gender>M</gender>
      <dob>2018-12-25</dob>
      <weight>120</weight>
      <neutered>Yes</neutered>
      <allergies>Peanut</allergies>
      <conditions>Obese</conditions>
      <medications>Yes</medications>
   </pet>
</pets>
```