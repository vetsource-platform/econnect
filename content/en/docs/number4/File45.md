---
title: "Step 4.5 of Retail Integration"
linkTitle: "Step 4.5 - Retail Prescription Sharing Information"
weight: 390
date: 2022-01-25
mermaid: true
description: >
  Retail Prescription Sharing Information
---
Prescription selection needs to be made on each line item level.

How will the prescription be delivered to the Veterinarian for approval?

<a id="PrescriptionDelivery" style="visibility:hidden;"></a>
<a href="../../images/PrescriptionDelivery.PNG"><img src="../../images/PrescriptionDelivery.PNG" align="center" style="max-width:100%;padding-top:20px;"></a>


The option to select the prescription preference must be based on the clinic selection as well as product.

Which values to display and its mapping is provided in this document [Prescription Sharing Options](https://docs.google.com/spreadsheets/d/1pun22GifwPT1QI0-k-9lO0bjXD2WfxLi/edit?usp=sharing&ouid=116278783390683079565&rtpof=true&sd=true)


**For RX Delivery Method Value:** Mail in Prescription.
```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>Mail in Prescription</rx-delivery-method>
      <req-ship-method>DEFAULT</req-ship-method>
   </line-item>
</line-items>
```

If a pet owner selects a clinic which doesn't accept written prescription (**written_rx_only = false**) then given value should default to “**I want the pharmacy to contact my Vet on my behalf**” on screen and pet owner should not have the option to select the alternative.

**For RX Delivery Method Value:** VetSource Contacts Vet.

```xml
<line-items>
   <line-item>
      <sku>28182999P5</sku>
      <quantity>1</quantity>
      <price>29</price>
      <vet-id>144955</vet-id>
      <pet-id>DOG007</pet-id>
      <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
      <req-ship-method>DEFAULT</req-ship-method>
   </line-item>
</line-items>
```
It may be helpful to provide a clickable ‘**tooltip**’ for pet owners experiencing this scenario to explain the reasoning. The Vetsource team can assist you with this text.

Once an **Order** is successfully placed we need to also provide correct prescription sharing instruction to Pet Owner.

**Guidelines for sharing prescription:**

**Step 1:**
Write your order number, phone number, and clinic name on your prescription.

**Step 2:**
For **nutrition** prescriptions:

Email to Rx@vetsource.com\
Send from a consistent outbound email like Rx@<partner email domain>.com\
Subject Line: Order XYZ: Nutrition Authorization file submitted!\
Body of Email:\
A new Nutrition Authorization file has been uploaded and sent to us.\
You will find it attached to this e-mail.\

Order number: x\
Customer name: y\
Phone number: z\
Email: .....\


or...

For **medication** prescription, mail the original to:

Attn Partner Name Approvals\
Vetsource\
17014 NE Sandy Blvd,\
Portland, OR 97230

**Please note:** By law the original medication prescription must be received by the pharmacy, photo-copies are not allowed.