---
title: "eConnect Integration Best Practices"
linkTitle: "Best Practices"
weight: 400.99
date: 2022-06-16
mermaid: true
description: >
   eConnect Integration Best Practices for API Integration
---
**How to authenticate with eConnect?**

You will use HTTP Basic Auth to authenticate via the following rules:

* All requests must include your API key. No password is required.
* All API requests must be made over HTTPS.
* Your API key will be provided as the Authorization header, prefixed with `Basic`.

type="Header" name="Authorization" value="Basic <Key>>  
type="Header" name="Content-Type" value="application/xml"  

**What not to send in API XML fields**
1. Avoid using special characters like :\
   a) ampersand '&'\
   b) less than sign '<'\
   c) greater than sign '>'\
   d) single quote '\
   e) double quote "

If mitigation for special characters is not done by Partner Dev Team then orders having above special characters  will not be received on Vetsource side and no API response will be sent / received for Order creation request.


**API Errors Troubleshooting**

Conventional HTTP response codes are used to indicate success or failure of an API request. In general,
1. Codes in the 2xx range indicate success
2. Codes in the 4xx range indicate an error that resulted from the provided information (e.g. a required parameter was missing)
3. Codes in the 5xx range indicate an error with the VetSource servers.

Not all errors map cleanly onto HTTP response codes, however (such as order validation failure). These types of errors will return a 503 error code. All errors return XML with a type (validation_error, invalid_request_error, or api_error) and message describing the particular problem.


**Different IDs in Data Shared by Vetsource**

Most objects in the Vetsource Platform have both an internal ID and an external ID. This allows easy synchronization between systems.

All APIs in the Vetsource Platform leverage IDs for lookups and relationships. Every object in the system is created with a unique internal Vetsource ID and exposes a flexible external ID for your usage. This allows you to refer to objects via either the internal or external IDs for your convenience.

If your PIM is integrated with Vetsource, the PIM IDs will already be stored as the external ID with the Vetsource objects.  In some cases when we receive an API call where the external ID is not found in the Vetsource system, we will create a new object with this external ID using the data in the API call.

**Order IDs Naming Convention in API calls**

External ID: `order-number` i.e. Order Number from Partner site  
Internal ID: `source-order-number` i.e. the Vetsource Order Number  

**Practice / Vet IDs Naming Convention in API calls**

External ID: `id` i.e. ID from Partner site  
Internal ID: `vetsource-clinic-id` i.e. Vetsource Practice ID  

**Product / SKU Naming Convention in API calls**

External ID: N/A  
Internal ID: `sku` i.e. is the Vetsource Product / SKU ID  

**Auto Cancellation of Aged Orders**
1. For Non WRX Prescriptions: If it is past 7 days and Nutrition, Medication prescriptions are yet not approved then Partner should cancel the Order and communicate the same to Pet Parents.
2. For WRX Prescriptions: If it is past 14 days and Nutrition, Medication prescriptions are yet not approved then Partner should cancel the Order and communicate the same to Pet Parents.
3. Partner should provide the cancel reason of TIMEOUT_CANCELLATION when they cancel orders due to time out.


















  


