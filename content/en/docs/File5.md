---
title: "Step 5 of Retail Integration"
linkTitle: "Step 5 -  Retail Order Creation"
weight: 399.9
date: 2022-06-16
mermaid: true
description: >
  Retail Order Creation
---
We need to capture and provide Pet Owner information in the Order creation API request body.

**Pet Owner Information**

```xml
    <order-number>TestOrderPartner001</order-number>
    <req-ship-method>TWODAY</req-ship-method>
    <client-id/>
    <client-name>James Bond</client-name>
    <client-phone>4154156789</client-phone>
    <client-email>testaccount@vetsource.com</client-email>
    <client-address-1>123 Main St.</client-address-1>
    <client-address-2/>
    <client-city>Orlando </client-city>
    <client-state>FL</client-state>
    <client-zip>32803</client-zip>
 ```

Order Data to be sent to Vetsource will be combination of Pet Owner information and below mentioned 5 sections from Step 4:

1. Retail Product Information (Step 4.1)
2. Retail Shipment Information (Step 4.2)
3. Retail Pet Information (Step 4.3)
4. Retail Veterinarian Information (Step 4.4)
5. Retail Prescription Sharing Information (Step 4.5)


**Final Order Request**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<order>
    <order-number>TestOrderPartner001</order-number>
    <req-ship-method>TWODAY</req-ship-method>
    <client-id/>
    <client-name>James Bond</client-name>
    <client-phone>4154156789</client-phone>
    <client-email>testaccount@vetsource.com</client-email>
    <client-address-1>123 Main St.</client-address-1>
    <client-address-2/>
    <client-city>Orlando </client-city>
    <client-state>FL</client-state>
    <client-zip>32803</client-zip>
    <line-items>
        <line-item>
            <sku>28182999P5</sku>
            <quantity>1</quantity>
            <price>29</price>
            <vet-id>144955</vet-id>
            <pet-id>DOG007</pet-id>
            <rx-delivery-method>VetSource Contacts Vet</rx-delivery-method>
        </line-item>
        <line-item>
            <sku>64360141BO</sku>
            <quantity>1</quantity>
            <price>39.99</price>
            <vet-id>144955</vet-id>
            <pet-id>DOG007</pet-id>
            <rx-delivery-method>Mail in Prescription </rx-delivery-method>
        </line-item>
    </line-items>
    <pets>
        <pet>
            <id>DOG007</id>
            <name>Goofy Bond</name>
            <species>Dog</species>
            <breed>Afghan</breed>
            <gender>M</gender>
            <dob>2018-12-25</dob>
            <weight>120</weight>
            <neutered>Yes</neutered>
            <allergies>Peanut</allergies>
            <conditions>Obese</conditions>
            <medications>Yes</medications>
        </pet>
    </pets>
    <vets>
        <vet>
            <id>PartnerVID001</id>
            <vetsource-clinic-id>144955</vetsource-clinic-id>
            <clinic-name>Test Hospital Lake Nona</clinic-name>
            <name></name>
            <address1>123 Main Street</address1>
            <address2>Ste 1234</address2>
            <city>Orlando</city>
            <state>FL</state>
            <zip>32801</zip>
            <phone>407-221-1234</phone>
            <fax>407-221-5678</fax>
        </vet>
    </vets>
</order>
```
  


















  


