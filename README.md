---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Update submodules for hugo themes: `git submodule update --init --recursive`
1. Install [Hugo](https://gohugo.io/getting-started/installing/). Note: you must install the "extended" version of hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/documentation/).

### Preview the site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from [Docsy](https://www.docsy.dev/)

## Updating the OpenAPI spec

To update the OpenAPI spec, replace `static/api/openapi.yaml` with the latest.
